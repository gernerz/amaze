//python -m SimpleHTTPServer
//Constants
const boardSize = 7; // Must be an odd number

//Globals
var mapCanvas;
var controlCanvas;
var tileArray;
var mapSize;
var paths;
var tileSize;
var playerTile;

//Tile Object
function Tile(t, r)
{
    this.t = t;
    this.r = r;
    this.item = 0;
    this.path = -1;
    // Returns the directions that are valid paths for this tile
    this.getPaths = function() 
    {
        var mod = 0;
        if(r != 0)
        {
            mod = (this.r/90);
        }
        if(this.t == 'tee')
        {
            return [(3+mod)%4,
                       (0+mod)%4,
                       (1+mod)%4];
        }
        else if(this.t == 'turn')
        {
            return [(3+mod)%4,
                       (0+mod)%4];
        }
        else if(this.t == 'straight')
        {
            if(this.r%180 == 0){return [0,2]}
            else{return [1,3]}
        }
        else{alert("Error: Invalid Tile Type!");}
    }
}

function initCanvas()
{
    mapSize = window.innerHeight * .65;
    document.getElementById('mapCanvas').style.width = mapSize + 'px';
    document.getElementById('mapCanvas').style.height = mapSize +'px';
    document.getElementById('controlCanvas').style.width = mapSize + 'px';
    document.getElementById('controlCanvas').style.height = (mapSize/7.5) +'px';
    mapCanvas = new Raphael(document.getElementById('mapCanvas'), mapSize, mapSize);
    controlCanvas = new Raphael(document.getElementById('controlCanvas'), mapSize, (mapSize/7.5));
    tileSize = (0.132*mapSize);
}

//Calculates which tiles are connected to eacother at a given time
function generatePaths()
{
    function mark(x,y,id)
    {
        var offset = (tileSize / 4) * 3;
        mapCanvas.circle(offset+tileSize*x,offset+tileSize*y,10).attr({fill: "yellow"});
        mapCanvas.text(offset+tileSize*x,offset+tileSize*y, id).attr({"font-size": 12});
    }

    function colorPath(x,y,id)
    {
        //Debug Log
        console.log("--------------")

        //Return if the location (x,y) is off of the board
        if(x>=boardSize || x<0 || y>=boardSize || y<0)
        {return}

        //Mark the tile with a tag for debugging purposes
        mark(x,y,id);
        //Debug Log
        console.log("[1]("+x+","+y+") -> paths:"+tileArray[x][y].getPaths());

        //Set this tiles Path id and remove it from the unvisited tile stack
        tileArray[x][y].path = id;
        tileStack.splice(tileStack.indexOf(x*10+y), 1);

        //For each path outlet on this tile check if it is connected to a
        //valid path outlet on the adjacent tile and recurse if it is else return.
        var dcnt = 0;
        tileArray[x][y].getPaths().forEach(function(path)
        {
            //Debug Log
            dcnt++;
            console.log('[2.'+dcnt+']('+x+','+y+') Checking Path: '+path);

            //If this tile has a path going up AND 
            //   is not on the top edge AND 
            //   tile above has a path going down
            if(path==0 && y>0 && (tileArray[x][y-1].getPaths().indexOf(2) != -1))
            {
                //If the tile is still in the unvisted tile stack
                if(tileStack.indexOf(x*10+y-1) != -1)
                {
                    console.log('   ('+x+','+y+') > Direction 0 is a valid path');
                    //Recurse to tile that is connected to this tile with a valid path
                    colorPath(x,y-1,id)
                }
                else
                {
                    console.log('   ('+x+','+y+') > Direction 1 is ALREADY VISITED');
                }
            }

            //If this tile has a path going right AND 
            //   is not on the right edge AND 
            //   tile to the right has a path going left
            if(path==1 && x<(boardSize-1) && (tileArray[x+1][y].getPaths().indexOf(3) != -1))
            {
                //If the tile is still in the unvisted tile stack
                if(tileStack.indexOf((x+1)*10+y) != -1)
                {
                    console.log('   ('+x+','+y+') > Direction 1 is a valid path');
                    colorPath(x+1,y,id)
                }
                else
                {
                    console.log('   ('+x+','+y+') > Direction 1 is ALREADY VISTIED');
                }
            }

            //If this tile has a path going down AND 
            //   is not on the bottom edge AND 
            //   tile under this has a path going left
            if(path==2 && y<(boardSize-1) && (tileArray[x][y+1].getPaths().indexOf(0) != -1))
            {
                //If the tile is still in the unvisted tile stack
                if(tileStack.indexOf(x*10+y+1) != -1)
                {
                    console.log('   ('+x+','+y+') > Direction 2 is a valid path');
                    colorPath(x,y+1,id)
                }
                else
                {
                    console.log('   ('+x+','+y+') > Direction 2 is ALREADY VISITED');
                }
            }

            //If this tile has a path going left AND 
            //   is not on the left edge AND 
            //   tile left this has a path going right
            if(path==3 && x>0 && (tileArray[x-1][y].getPaths().indexOf(1) != -1))
            {
                //If the tile is still in the unvisted tile stack
                if(tileStack.indexOf((x-1)*10+y) != -1)
                {
                    console.log('   ('+x+','+y+') > Direction 3 is a valid path');
                    colorPath(x-1,y,id)
                }
                else
                {
                    console.log('   ('+x+','+y+') > Direction 3 is ALREADY VISITED');
                }
            }
        });
        return
    }

    //Initialize TileStack
    var tileStack  = [];
    for(var i=0; i<boardSize; i++)
    {
        for(var j=0; j<boardSize; j++)
        {
            tileStack.push(i*10+j);
        }
    }

    //Color paths until all tiles have been covered
    var id = 0;
    // while(tileStack.length > 0)
    // {
        var seed = tileStack.pop();
        colorPath(Math.floor(seed/10), seed%10, id);
        console.log("##### ID "+id+" Complete #####");
        id = id + 1;
    // }
}

//Draws the static UI elements
function drawUI()
{
    var offset = tileSize/4;

    mapCanvas.image("img/topGutter.png", (offset + tileSize), 0, tileSize, (tileSize/4))
        .click(function(){pushTile('cd',1)});
    mapCanvas.image("img/topGutter.png", (offset + 3*tileSize), 0, tileSize, (tileSize/4))
        .click(function(){pushTile('cd',3)});
    mapCanvas.image("img/topGutter.png", (offset + 5*tileSize), 0, tileSize, (tileSize/4))
        .click(function(){pushTile('cd',5)});

    mapCanvas.image("img/rightGutter.png", (mapSize - tileSize/4), (offset + tileSize), (tileSize/4), tileSize)
        .click(function(){pushTile('rl',1)});
    mapCanvas.image("img/rightGutter.png", (mapSize - tileSize/4), (offset + 3*tileSize), (tileSize/4), tileSize)
        .click(function(){pushTile('rl',3)});
    mapCanvas.image("img/rightGutter.png", (mapSize - tileSize/4), (offset + 5*tileSize), (tileSize/4), tileSize)
        .click(function(){pushTile('rl',5)});

    mapCanvas.image("img/bottomGutter.png", (offset + tileSize), (mapSize-tileSize/4), tileSize, (tileSize/4))
        .click(function(){pushTile('cu',1)});
    mapCanvas.image("img/bottomGutter.png", (offset + 3*tileSize), (mapSize-tileSize/4), tileSize, (tileSize/4))
        .click(function(){pushTile('cu',3)});
    mapCanvas.image("img/bottomGutter.png", (offset + 5*tileSize), (mapSize-tileSize/4), tileSize, (tileSize/4))
        .click(function(){pushTile('cu',5)});

    mapCanvas.image("img/leftGutter.png", 0, (offset + tileSize), (tileSize/4), tileSize)
        .click(function(){pushTile('rr',1)});
    mapCanvas.image("img/leftGutter.png", 0, (offset + 3*tileSize), (tileSize/4), tileSize)
        .click(function(){pushTile('rr',3)});
    mapCanvas.image("img/leftGutter.png", 0, (offset + 5*tileSize), (tileSize/4), tileSize)
        .click(function(){pushTile('rr',5);});

    controlCanvas.image("img/rightArrow.png", (tileSize*2), 0, tileSize, tileSize)
        .click(function(){
            playerTile.r = (playerTile.r + 90) % 360;
            controlCanvas.image("img/"+playerTile.t+".png", tileSize/4+tileSize*3, 0, tileSize, tileSize)
                .transform("r"+playerTile.r);
            console.log(">>> Valid Paths: " + playerTile.getPaths());
        });
    controlCanvas.image("img/leftArrow.png", (offset*2+tileSize*4), 0, tileSize, tileSize)
        .click(function(){
            playerTile.r = (playerTile.r + 270) % 360;
            controlCanvas.image("img/"+playerTile.t+".png", tileSize/4+tileSize*3, 0, tileSize, tileSize)
                .transform("r"+playerTile.r);
            console.log(">>> ["+playerTile.r+"] Valid Paths: " + playerTile.getPaths());
        });
}

//Draws graphics
function draw()
{  
    mapCanvas.clear();
    controlCanvas.clear();
    drawUI();
    //Draw Tiles
    var offset = tileSize/4;
    for (var i = 0; i < boardSize; i++) 
    { 
        for (var j = 0; j < boardSize; j++) 
        { 
            var typ = tileArray[i][j].t;
            var rot = tileArray[i][j].r;
            var tile = mapCanvas.image("img/"+typ+".png", (offset + i*tileSize), (offset + j*tileSize), tileSize, tileSize);
            tile.transform("r"+rot);
        }
    }
    controlCanvas.image("img/"+playerTile.t+".png", tileSize/4+tileSize*3, 0, tileSize, tileSize)
        .transform("r"+playerTile.r);
}

//Initialize Game Board
function gameInit()
{
    //Randomly permutes passed array
    function shuffle(array) 
    {
      var m = array.length, t, i;
      // While there remain elements to shuffle…
      while (m) {
        // Pick a remaining element…
        i = Math.floor(Math.random() * m--);
        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
      }
      return array;
    }
    //Set starting player tile
    playerTile = new Tile("straight", 0);

    //Initialize map datastructure
    tileArray = [];
    for (var i  = 0; i < boardSize; i++)
    {
        tileArray[i] = [];
    }

    //Add static tiles
    // tileArray[0][0] = new Tile("turn", 90);
    tileArray[0][0] = new Tile("turn", 90);
    tileArray[2][0] = new Tile("tee", 180);
    tileArray[4][0] = new Tile("tee", 180);
    tileArray[6][0] = new Tile("turn", 180);
    tileArray[0][2] = new Tile("tee", 90);
    tileArray[2][2] = new Tile("tee", 90);
    tileArray[4][2] = new Tile("tee", 180);
    tileArray[6][2] = new Tile("tee", 270);
    tileArray[0][4] = new Tile("tee", 90);
    tileArray[2][4] = new Tile("tee", 0);
    tileArray[4][4] = new Tile("tee", 270);
    tileArray[6][4] = new Tile("tee", 270);
    tileArray[0][6] = new Tile("turn", 0);
    tileArray[2][6] = new Tile("tee", 0);
    tileArray[4][6] = new Tile("tee", 0);
    tileArray[6][6] = new Tile("turn", 270);


    //Randomize dynamic tiles
    var numTiles = 33;
    var tileStack = [numTiles];
    for(var i = 1; i <= numTiles; i++)
    {
        var randRot = 90 * Math.floor(Math.random() * (3 + 1));
        if(i <= 11)
        {
            tileStack[tileStack.length] = new Tile("turn", randRot);
        }
        else if(i <= 22)
        {
            tileStack[tileStack.length] = new Tile("tee", randRot);
        }
        else
        {
            tileStack[tileStack.length] = new Tile("straight", randRot);
        } 
    }
    tileStack.shift(); //removes first elemt which is undefined
    shuffle(tileStack);

    //Place randomized tiles on dynamic tile positions
    for (var i = 0; i < boardSize; i++) 
    { 
        for (var j = 0; j < boardSize; j++) 
        { 
            if((j%2 == 1) || (i%2 == 1))
            {
                tileArray[i][j] = tileStack.shift();
            }
        }
    }
}


window.addEventListener('load', function()
{
    // Initialize
    initCanvas();
    gameInit();
    drawUI();
    draw();
    generatePaths()
    console.log("All Loaded!");
}, false);

window.addEventListener('resize', function()
{
    initCanvas();
    drawUI();
}, false);

function pushTile(loc, idx)
{
    if(loc == 'cd')
    {
        var tempTile = tileArray[idx][(boardSize-1)];
        for(var i=(boardSize - 1); i > 0; i--)
        {
            tileArray[idx][i] = tileArray[idx][i-1];
        }
        tileArray[idx][0] = playerTile;
        playerTile = tempTile;
        draw();
    }
    else if(loc == 'cu')
    {
        var tempTile = tileArray[idx][0];
        for(var i=1; i < boardSize; i++)
        {
            tileArray[idx][i-1] = tileArray[idx][i];
        }
        tileArray[idx][(boardSize-1)] = playerTile;
        playerTile = tempTile;
        draw();
    }
    else if(loc == 'rr')
    {
        var tempTile = tileArray[(boardSize-1)][idx];
        for(var i=(boardSize - 1); i > 0; i--)
        {
            tileArray[i][idx] = tileArray[i-1][idx];
        }
        tileArray[0][idx] = playerTile;
        playerTile = tempTile;
        draw();

    }
    else if(loc == 'rl')
    {
        var tempTile = tileArray[0][idx];
        for(var i=1; i < boardSize; i++)
        {
            tileArray[i-1][idx] = tileArray[i][idx];
        }
        tileArray[(boardSize-1)][idx] = playerTile;
        playerTile = tempTile;
        draw();
    }
    else
    {
        throw("@PushTile - Unknown Push Direction");
    }

    generatePaths();
}
